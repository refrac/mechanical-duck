# Mechanical-duck
Ce programme est un bot discord d'aide a la moderation qui permet d'attribuer temporairement un role.

#### configuration:
pour configurer le bot renommez le fichier config.json.example (ou config.json.fr.example) en config.json et modifiez les valeurs suivantes:
* token: rentrez ici le [token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token) de votre bot discord.
* save_path: le chemin absolu ou relatif du fichier dans lequel seront stockees les donnees persistantes (peut etre laisse comme tel)
* timezone: le fuseau horaire des utilisateurs du serveur (par exemple pour Paris: 2)
* role_moderation: l'ID du role de la moderation (pour trouver cet ID, activez le mode developpeur de discord (User settings > appearance > developer mode) puis cliquez droit sur le role desire et selectionnez "copy ID")
 
Vous devez ensuite configurer les commands et les dialogs.

##### les commands:

premierement vous devez configurer les differentes commandes qui vous serviront a deplacer les membres vers les differents role, pour faire ceci, vous devez editer le tableau command_list en duplicant l'element deja present autant de fois que de commandes.

editez ensuite chaque elements en indiquant pour chacuns:

* le mot cle qui definira cette commandes (par exemple: "!deplacer")
* l'ID du role vers lequel les membres seront deplaces

ainsi que les dialogs, c'est a dire les reponses du bot personnalise a cette commandes

vous devrez aussi editer la commande cancel en bas du fichier qui vous servira plus tard a annuler un deplacement

##### les dialogs:

vous pouvez editer les dialogs pour les adapter (a votre langue par exemple), il y a deux types de dialogs:

 * les mot cles de commandes c'est a dire les mots que vous utiliserez dans vos commandes, pour ceux-ci vous pouvez indiquer plusieurs synonymes dans le tableau.
 * les reponses du bot a vos commandes selon les cas.
 


#### syntaxe:

`<commande> <pseudo> [pour | pendant <nombre> <secondes | minutes | heures | jours | semaines | mois>]`

#### exemples:

`!chatier @zaphod_beeblebrox pour 1 minute`

`!deplacer @marvin pendant 30 secondes`

`!deplacer @arthur_dent`

`!annuler @marvin`
