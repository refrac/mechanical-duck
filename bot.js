// Import the discord.js module
const Discord = require('discord.js');
const fs = require('fs');
const http = require('http');
const config = require("./config/config.json");
var schedule = require('node-schedule');


// Create an instance of a Discord client
const client = new Discord.Client();

var file_path = config.save_path;
var command_list = config.command_list;
var role_moderation = config.role_moderation;
var general_dialog = config.general_dialog;
var cancel_dialog = config.cancel_dialog;

if (!fs.existsSync(file_path))
{
    fs.writeFileSync(file_path, "[]");
}
var data = fs.readFileSync(file_path);
if (data)
{
    var   task_list = JSON.parse(data);
}


//console.log(task_list);
client.login(config.token);

client.on('ready',  async () => {
    console.log('I am ready!');
    for (const e of task_list) {
        e.guild.members = null;
        var guild = await (new Discord.Guild(client, e.guild)).fetch();
        var member = await guild.members.fetch(e.member.userID).catch(err=>{console.log("unknown member")});
	if (member)
	    e.member = member;
	else
	    e.member = null;
        e.job = schedule.scheduleJob(new Date(e.date), function(){end_chatiment(e)});
    }
});

function end_chatiment(task) {
    task.member.roles
        .set(task.roles)
        .then(() => {
        })
        .catch(err => {
            console.error(err);
            throw err;
        });
    console.log("chatiment ended");
    task_list.forEach((e, index) => {
        if (e === task)
        {
            task_list.splice(index, 1);
        }
    });
    fs.writeFile(file_path, JSON.stringify(task_list), function (err) {
        console.log(err)
    });
}

client.on('guildMemberAdd', member => {
	task_list.forEach(e => {
		if (!e.member)
			return;
		if (e.member.user.id === member.user.id)
		{
			member.roles.set(e.current_roles)
			.then(() => {
        		})
	        	.catch(err => {
        		    console.error(err);
        		    throw err;
        		});
		}
	})
})

client.on('message', message => {
    var is_moderator = false;
    if (!message.guild) return;
    var member = message.guild.member(message.author);
    if (!member) return;
    (member.roles.cache.array()).forEach(e => {
        is_moderator |= role_moderation.includes(e.id);
    });

    if (message.content == "!rolelist") {
        if (!is_moderator && role_moderation != "")
        {
            message.reply(general_dialog.missing_permission);
            return;
        }
        var tmp = "\n";
        (message.guild.roles.cache.array()).forEach((content, index) => {if (index != 0){tmp += (content.name + ": " + content.id + "\n")}});
        message.reply(tmp);
    }
    role_chatiment = null;
    var dialog = null;
    command_list.forEach(content => {if ((message.content.split(' '))[0] === content.command){
        role_chatiment = content.role;
        dialog = content.dialog;
    }});
    if (role_chatiment) {
        if (!is_moderator)
        {
            message.reply(general_dialog.missing_permission);
            return;
        }
        const user = message.mentions.users.first();

        var time = 1000000000000;
        // If we have a user mentioned
        if (user) {
            const member = message.guild.member(user);
            // Now we get the member from the user
            var factor = 1000;
            (message.content.split(' ')).forEach( (word, index, array) => {
                if (general_dialog.for.includes(word.toLowerCase()))
                {
                    time = Math.max(0, parseInt(array[index + 1]));
                    if (array[index + 2])
                    {
                        word = array[index + 2].toLowerCase();
                        console.log(word);
                        if (general_dialog.second.includes(word))
                           factor = 1000;
                       else if (general_dialog.minute.includes(word))
                           factor = 60000;
                       else if (general_dialog.hour.includes(word))
                           factor = 3600000;
                       else if (general_dialog.day.includes(word))
                           factor = 3600000 * 24;
                       else if (general_dialog.week.includes(word))
                            factor = 3600000 * 24 * 7;
                       else if (general_dialog.month.includes(word))
                           factor = 3600000 * 24 * 30;
                    }
                    else
                        factor = 3600000 * 24;
                }
            });

            // If the member is in the guild
            if (member) {
                var task = new Object();
                task.member = member;
                task.guild = member.guild;
                var role_list = [];
                task.roles = role_list;
                var already_punished = false;
                (member.roles.cache.array()).forEach(role => {
                   if (role.name != "@everyone"){
                       role_list.push(role.id);
                   }
                   command_list.forEach(content => {
                       if (role.id === content.role)
                   {
                       message.reply(dialog.already_moved);
                       already_punished = true;
                   }})

                });
                console.log(role_list);
                if (!already_punished) {
                    member.roles
                        .set([role_chatiment])
                        .then(() => {
                            message.reply(user.tag + dialog.success + (new Date(task.date + config.timezone * 3600000) + "").substring(0, 24) + ".");
                        })
                        .catch(err => {
                            message.reply('Error: ', err);
                            console.error(err);
                            throw err;
                        });
                    if (time != 0) {
                        task.date = (new Date()).getTime() + time * factor;
                        task.job = schedule.scheduleJob(new Date(task.date), function(){end_chatiment(task)});
			task.current_roles=[role_chatiment]
                        task_list.push(task);
                        console.log("task scheduled");
                        fs.writeFile(file_path, JSON.stringify(task_list), function (err) {
                            console.log(err)
                        });
                    }
                }
            } else {
                message.reply(dialog.not_in_guild);
            }
        } else {
            message.reply(dialog.mention_missing);
        }
    }
    if (cancel_dialog.cancel_command.includes((message.content.split(' '))[0])){
        if (!is_moderator)
        {
            message.reply(general_dialog.missing_permission);
            return;
        }
        const user = message.mentions.users.first();

        // If we have a user mentioned
        if (user) {
            const member = message.guild.member(user);

            // If the member is in the guild
            if (member) {
                var canceled = 0;
                task_list.forEach(content => {
                    if ((content.member.id + "") === (member.id+ ""))
                    {
                        end_chatiment(content);
                        content.job.cancel();
                        message.reply(cancel_dialog.success);
                        canceled = 1;
                    }
                });
                if (!canceled){
                    message.reply(cancel_dialog.failure);
                }
            } else {
                message.reply("not in guild");
            }
        } else {
            message.reply("please mention a member");
        }

    }
});




